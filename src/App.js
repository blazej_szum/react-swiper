import React, { Component } from 'react';
import './App.scss'
import Person from './Person'
import persons from './persons'
import pick from 'lodash/fp/pick'

const slideWidth = 600;

export default class App extends Component {

    componentWillMount() {
        this.setState({swiperMode: false});
    }

    render() {
        return (
            <div>
                <div className="header">Meet our team</div>
                <div className="person-container" ref={(ref) => this.personContainer = ref}>
                    {persons.map((person, index) =>
                        <Person
                            person={person}
                            key={index}
                            swiperMode={this.state.swiperMode}
                            onClick={() => this.onClick(index)}
                        />
                    )}
                </div>
            </div>

        );
    }

    onClick = (selectedSlideIndex) => {
        if (this.state.swiperMode) {
            if (this.selectedSlideIndex > selectedSlideIndex) {
                this.prevSlide();
            } else if (this.selectedSlideIndex < selectedSlideIndex) {
                this.nextSlide();
            } else if (this.selectedSlideIndex === selectedSlideIndex) {
                this.noSwyper();
            }
            return;
        }
        this.selectedSlideIndex = selectedSlideIndex;
        this.swyper(selectedSlideIndex);
    };




    swyper = (selectedSlideIndex) => {
        this.offsets = this.offsets || Array.from(this.personContainer.childNodes).map( element => { return {
            offsetTop: element.offsetTop - this.personContainer.offsetTop,
            offsetLeft: element.offsetLeft - this.personContainer.offsetLeft
        }});
        this.personContainer.style.position = 'relative';
        this.personContainer.style.overflow = 'hidden';
        this.personContainer.style.perspective = '2000px';

        this.personContainer.style.height = '600px';
        this.personContainer.childNodes.forEach((element, i) => {
            element.style.transitionProperty = 'all';
            element.style.transitionTimingFunction = 'ease-out';
            element.style.position = 'absolute';
        });
        setTimeout(this.slide);
        this.setState({swiperMode: true});
    };

    noSwyper = () => {
        this.personContainer.style.position = '';
        this.personContainer.style.overflow = '';
        this.personContainer.style.height = '';
        this.personContainer.childNodes.forEach(element => {
            element.style.opacity = '0';
            element.style.transitionProperty = 'opacity';
            element.style.opacity = '1';
            element.style.transform = '';
            element.style.left = '';
            element.style.position = '';
        });
        this.setState({swiperMode: false});
    };

    nextSlide = () => {
        if (this.selectedSlideIndex >= (persons.length - 1)) {
            return;
        }
        this.selectedSlideIndex++;
        this.slide();
    };

    prevSlide = () => {
        if (this.selectedSlideIndex <= 0) {
            return;
        }
        this.selectedSlideIndex--;
        this.slide(slideWidth);
    };

    slide = () => {
        this.personContainer.childNodes.forEach((element, i) => {
            const diff = (window.innerWidth - slideWidth) / 2 - 80;
            const left = (i - this.selectedSlideIndex) * slideWidth + diff;
            const rotate = (i - this.selectedSlideIndex)*30;
            const scale = this.selectedSlideIndex === i ? 1 : .5;
            element.style.position = 'absolute';
            element.style.left =`${left}px`;
            element.style.transform = `scale(${scale}) rotateY(${rotate}deg)`;
            element.style.opacity = this.selectedSlideIndex === i ? '1' : '0.2';
        });
    }

}
