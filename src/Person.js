import React, { Component } from 'react';
import './Person.scss'

export default class Person extends Component {

    render() {
        console.log('render');
        const {name, role, location} = this.props.person;
        return (
            <div className={`person ${this.props.swiperMode ? 'swiperMode' : ""}`} onClick={this.props.onClick}>
                <img className="person__icon" width="210px" height="210px" src={`https://source.unsplash.com/random/300x300?for=${name}`} alt="avatar"/>
                <div className="person__name">
                    {name}
                </div>
                <div className="person__role">
                    {role}
                </div>
                <div className="person__location">
                    {location}
                </div>
                <div className="person__description">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
            </div>
        );
    }
}
